<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Conferencias por asistir</title>
    <link rel="stylesheet" href="../css/estilo.css">
  </head>
  <body>
    <h1 class="titulo add-conf-titu">Mis conferencias<br>por asistir. </h1>
    <div class="contain-cards">
      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-7" name="add-conf4" value="true">
        <label for="check-7"></label>

        <img src="../img/tarjetas-conf/ventas.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>

      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-8" name="add-conf5" value="true">
        <label for="check-8"></label>

        <img src="../img/tarjetas-conf/ventas.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>

      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-9" name="add-conf6" value="true">
        <label for="check-9"></label>

        <img src="../img/tarjetas-conf/ventas.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>
    </div>

      <h1 class="titulo add-conf-titu">Mis talleres <br> por asisitir. </h1>

      <div class="contain-cards">
        <div class="tarjeta-add-conf">

          <input type="checkbox" id="check-10" name="add-tall4" value="true">
          <label for="check-10"></label>

          <img src="../img/tarjetas-conf/ventas.jpg" alt="">
          <h2>
            Inteligencia Artificial
          </h2>
          <p>
            Por: Autor Apellido Apellido
          </p>
          <p>
            Hora: hh/mm
          </p>
          <p>
            Lugar: Auditorio I
          </p>
        </div>

        <div class="tarjeta-add-conf">

          <input type="checkbox" id="check-11" name="add-tall5" value="true">
          <label for="check-11"></label>

          <img src="../img/tarjetas-conf/linux.jpg" alt="">
          <h2>
            Inteligencia Artificial
          </h2>
          <p>
            Por: Autor Apellido Apellido
          </p>
          <p>
            Hora: hh/mm
          </p>
          <p>
            Lugar: Auditorio I
          </p>
        </div>

        <div class="tarjeta-add-conf">

          <input type="checkbox" id="check-12" name="add-tall6" value="true">
          <label for="check-12"></label>

          <img src="../img/tarjetas-conf/bolsa.jpg" alt="">
          <h2>
            Inteligencia Artificial
          </h2>
          <p>
            Por: Autor Apellido Apellido
          </p>
          <p>
            Hora: hh/mm
          </p>
          <p>
            Lugar: Auditorio I
          </p>
        </div>
      </div>

    <div class="button-cont">
      <button type="button" class="btn-conferencias">Añadir màs</button>

      <!-- para desactivar
      <button type="button" class="disab">Añadir màs</button> -->

      <button class="btn-red" type="button">Eliminar</button>
    </div>

  </body>
</html>
