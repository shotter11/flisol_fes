<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Mis conferencias</title>
    <link rel="stylesheet" href="../css/estilo.css">
  </head>
  <body>
    <div class="contain-conf-begin">
      <div class="titulo">
        <h1>Mis conferencias <br> por asistir: </h1>
      </div>

      <p class="begin-conf">Bienvenido, aun no tienes <br> conferencias por asistir.</p>

      <input type="submit" name="" value="Añadir" class="btn-conferencias">
    </div>

  </body>
</html>
