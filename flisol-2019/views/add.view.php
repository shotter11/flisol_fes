<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Añadir conferencias</title>
    <link rel="stylesheet" href="../css/estilo.css">
  </head>
  <body>
    <h1 class="titulo add-conf-titu">Añade las conferencias <br> a las que asistiras. </h1>
    <div class="contain-cards">
      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-1" name="add-conf1" value="true">
        <label for="check-1"></label>

        <img src="../img/tarjetas-conf/ventas.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>

      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-2" name="add-conf2" value="true">
        <label for="check-2"></label>

        <img src="../img/tarjetas-conf/linux.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>

      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-3" name="add-conf3" value="true">
        <label for="check-3"></label>

        <img src="../img/tarjetas-conf/bolsa.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>
    </div>


    <h1 class="titulo add-conf-titu">Añade los talleres <br> a las que asistiras. </h1>

    <div class="contain-cards">
      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-4" name="add-tall1" value="true">
        <label for="check-4"></label>

        <img src="../img/tarjetas-conf/ventas.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>

      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-5" name="add-tall2" value="true">
        <label for="check-5"></label>

        <img src="../img/tarjetas-conf/linux.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>

      <div class="tarjeta-add-conf">

        <input type="checkbox" id="check-6" name="add-tall6" value="true">
        <label for="check-6"></label>

        <img src="../img/tarjetas-conf/bolsa.jpg" alt="">
        <h2>
          Inteligencia Artificial
        </h2>
        <p>
          Por: Autor Apellido Apellido
        </p>
        <p>
          Hora: hh/mm
        </p>
        <p>
          Lugar: Auditorio I
        </p>
      </div>
    </div>

    <div class="button-cont">
      <button type="button" name="button" class="btn-conferencias">Guardar</button>
      <button type="button" name="button" class="btn-conferencias">Consultar</button>
    </div>
  </body>
</html>
